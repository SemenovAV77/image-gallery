import { useRef, useState, useEffect } from 'react';

export const useImageLoaded = () => {
  const [loaded, setLoaded] = useState(false);

  const onLoad = () => {
    setLoaded(true);
  };

  return { loaded, onLoad };
};
