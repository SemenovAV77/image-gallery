import React, { useEffect, useState } from 'react';

import { IPhoto } from 'Api/photos/hooks';
import { Photos } from 'Pages/gallery/index';
import './GroupPhotos.scss';

interface IProps {
  photos: IPhoto[][];
  tag: string;
  setTagInInput: (tag: string) => void;
}

const GroupPhotos: React.FC<IProps> = ({ photos, tag, setTagInInput }) => {
  const [photosFiltred, setPhotosFiltred] = useState<IPhoto[][]>([]);
  useEffect(() => {
    const filtred = photos.filter((photo) => {
      if (photo.findIndex((el) => el.tag === tag.toLowerCase()) > -1) {
        return true;
      } else return false;
    });
    setPhotosFiltred(filtred);
  }, [photos]);
  return (
    <div className="group">
      <div className="group__title">{tag}</div>
      <Photos photos={photosFiltred} setTagInInput={setTagInInput} />
    </div>
  );
};

export default GroupPhotos;
