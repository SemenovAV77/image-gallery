import React, { forwardRef, useEffect, useState } from 'react';
import { Formik, Form, FormikHelpers } from 'formik';
import * as yup from 'yup';
import { v4 } from 'uuid';
import { useToasts } from 'react-toast-notifications';

import { FormikTextField } from 'Components/formik-fields/FormikTextField';
import { Button } from 'Components/common';
import './FormLoader.scss';

const initialValues = { tag: '' };

const validationSchema = yup.object().shape({
  tag: yup.string().required(),
});

interface IProps {
  onSubmit: (tag: string, uuid: string) => Promise<any>;
  ref: any;
}

const FormLoader: React.FC<IProps> = forwardRef((props, ref) => {
  const { addToast } = useToasts();
  const [isDelay, setIsDelay] = useState(false);
  let timerDel: Array<ReturnType<typeof setTimeout>> = [];

  function delay() {
    return new Promise((resolve) => setTimeout(resolve, 5000));
  }

  const randomString = (len: number) => {
    const charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    let randomString = '';
    for (let i = 0; i < len; i++) {
      let randomPoz = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
  };
  const getRandomInt = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  };

  const submitHandler = (params: typeof initialValues, formikHelpers: FormikHelpers<any>) => {
    const uuidVal = v4();
    let noTags = true;
    const promises: any[] | PromiseLike<any[]> = [];
    const decompose = params.tag.split(',');

    (async () => {
      isDelay && (await delay());
      for (let item = 0; item < decompose.length; item++) {
        if (decompose[item].length > 0) {
          noTags = false;
          if (decompose[item] === 'delay') {
            let promise = props.onSubmit(randomString(getRandomInt(1, 11)), uuidVal);
            promises.push(promise);
            setIsDelay(true);
            let timerItem: ReturnType<typeof setTimeout> = setTimeout(
              () => setIsDelay(false),
              5000,
            );
            timerDel = [...timerDel, timerItem];
            await delay();
          } else {
            let promise = props.onSubmit(decompose[item], uuidVal);
            promises.push(promise);
          }
        }
      }
    })();
    Promise.all(promises.map((promise) => promise)).finally(() => {
      formikHelpers.setSubmitting(false);
    });
    noTags && formikHelpers.setSubmitting(false);
  };

  const click = (tag: string) => {
    tag === '' && addToast('Введите тег', { appearance: 'error', autoDismiss: true });
  };

  useEffect(() => {
    return () => timerDel.forEach((item) => clearTimeout(item));
  }, []);

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={submitHandler}
      validationSchema={validationSchema}
      innerRef={ref as any}
    >
      {({ isSubmitting, values }) => (
        <Form>
          <div className="form-wrapper">
            <FormikTextField name="tag" placeholder="введите тег" />
            <Button type="submit" disabled={isSubmitting} onClick={() => click(values.tag)}>
              {isSubmitting ? 'Загрузка..' : 'Загрузить'}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  );
});

export default FormLoader;
