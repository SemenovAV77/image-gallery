import React from 'react';

import { FormLoader, GroupPhotos, Photos } from 'Pages/gallery/index';
import { usePhotos } from 'Api/photos/hooks';
import { Button } from 'Components/common';
import './Gallery.scss';

const Gallery = () => {
  const {
    photos,
    sendRequest,
    tags,
    isGrouped,
    changeGrouping,
    clearPhotos,
    refInput,
    setTagInInput,
  } = usePhotos();

  return (
    <div className="container">
      <div className="input-wrapper">
        <FormLoader onSubmit={sendRequest} ref={refInput} />
        <Button variant="secondary" onClick={clearPhotos}>
          Очистить
        </Button>
        <Button variant="primary" onClick={changeGrouping}>
          {!isGrouped ? 'Группировать' : 'Разгруппировать'}
        </Button>
      </div>
      {!isGrouped ? (
        <Photos photos={photos} setTagInInput={setTagInInput} />
      ) : (
        <>
          {tags.map((tag, index) => (
            <GroupPhotos key={index} photos={photos} tag={tag} setTagInInput={setTagInInput} />
          ))}
        </>
      )}
    </div>
  );
};

export default Gallery;
