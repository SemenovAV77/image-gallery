import React from 'react';

import { IPhoto } from 'Api/photos/hooks';
import { Photo } from 'Pages/gallery/index';
import './Photos.scss';

interface IProps {
  photos: IPhoto[][];
  setTagInInput: (tag: string) => void;
}

const Photos: React.FC<IProps> = ({ photos, setTagInInput }) => {
  return (
    <div className="photos">
      {photos &&
        photos.map((photo, index) => (
          <Photo key={index} photo={photo} setTagInInput={setTagInInput} />
        ))}
    </div>
  );
};

export default Photos;
