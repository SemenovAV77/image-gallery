import React from 'react';
import cn from 'classnames';

import { useImageLoaded } from 'hooks/useImageLoaded';
import { IPhoto } from 'Api/photos/hooks';

type TProps = {
  photo: IPhoto[];
  setTagInInput: (tag: string) => void;
};

const Photo: React.FC<TProps> = ({ photo, setTagInInput }) => {
  const { loaded, onLoad } = useImageLoaded();
  return (
    <div className="photos-item">
      <div className={cn('loading-block', { 'loading-block--hide': loaded })}>Loading...</div>
      {photo.map((item, index) => (
        <img
          onClick={() => {
            setTagInInput(item.tag);
          }}
          key={index}
          src={item.imageUrl}
          alt={`photo-${item.imageUrl}`}
          onLoad={onLoad}
          className={cn('loading-image', { 'loading-image--show': loaded })}
        />
      ))}
    </div>
  );
};

export default Photo;
