import axios, { AxiosRequestConfig } from 'axios';
import config from 'config.json';

export const transport = {
  async get(params?: AxiosRequestConfig['params']) {
    const paramWithKey = { api_key: process.env.REACT_APP_GIPHY_API_KEY, ...params };
    const response = await axios
      .get(config.serverUrl, { params: paramWithKey })
      .catch((error: any) => {
        return Promise.reject(error);
      });
    return response.data;
  },
};
