import React from 'react';
import { Field, FieldProps } from 'formik';

import { Input } from 'Components/common';
import { RegularExp } from 'entities/regexp';

interface IFormikTextFieldProps {
  name: string;
  type?: 'text';
  placeholder?: string;
}

export const FormikTextField: React.FC<IFormikTextFieldProps> = ({ name, ...restProps }) => (
  <Field name={name}>
    {(fieldProps: FieldProps) => {
      const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        let cropValue = (e.target.value as string).replace(RegularExp.FORMIKFIELD, '');
        fieldProps.form.setFieldValue(name, cropValue);
      };
      return <Input onChange={onChange} value={fieldProps.field.value} {...restProps} />;
    }}
  </Field>
);
