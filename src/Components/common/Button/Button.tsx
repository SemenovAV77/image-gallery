import React from 'react';
import cn from 'classnames';

import './Button.scss';

interface IProps {
  disabled?: boolean;
  type?: 'submit';
  onClick?: () => void;
  variant?: 'default' | 'primary' | 'secondary';
}

const Button: React.FC<IProps> = ({
  children,
  variant = 'default',
  disabled,
  type,
  onClick,
  ...restProps
}) => {
  return (
    <div
      className={cn('buttonBase', {
        primary: variant === 'primary',
        secondary: variant === 'secondary',
      })}
    >
      <button type={type} disabled={disabled} onClick={onClick} {...restProps}>
        {children}
      </button>
    </div>
  );
};

export default Button;
