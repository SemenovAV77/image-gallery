import React from 'react';

import './Input.scss';

interface IProps {
  value: string;
  placeholder?: string;
  type?: 'text';
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input: React.FC<IProps> = ({ value, placeholder, type = 'text', ...restProps }) => {
  return (
    <div className="inputBase">
      <input type={type} placeholder={placeholder} value={value} {...restProps} />
    </div>
  );
};

export default Input;
