import { useRef, useState } from 'react';
import { PhotosAPI } from './api';
import { useToasts } from 'react-toast-notifications';

export interface IPhoto {
  imageUrl: string;
  tag: string;
  uuid: string;
}

export const usePhotos = () => {
  const [photos, setPhotos] = useState<IPhoto[][]>([]);
  const [tags, setTags] = useState<string[]>([]);
  const [isGrouped, setIsGrouped] = useState<boolean>(false);
  const { addToast } = useToasts();
  const refInput = useRef(null) as any;

  const sendRequest = (tag: string, uuid: string) => {
    return PhotosAPI.getPhotos({ tag })
      .then((response) => {
        if (response.data.length === 0) {
          addToast(`По тэгу \'${tag}\' ничего не найдено`, {
            appearance: 'warning',
            autoDismiss: true,
          });
        } else {
          setTags((prevState) => {
            if (prevState.findIndex((el) => el === tag.toLowerCase()) > -1) return [...prevState];
            else return [...prevState, tag.toLowerCase()];
          });
          setPhotos((prevState) => {
            let newState = [...prevState];
            let finded = false;
            const state = newState.map((item) => {
              if (item.findIndex((el) => el.uuid === uuid) > -1) {
                finded = true;
                return [
                  ...item,
                  { imageUrl: response.data.image_url, tag: tag.toLowerCase(), uuid },
                ];
              } else return item;
            });
            return !finded
              ? [
                  ...prevState,
                  [{ imageUrl: response.data.image_url, tag: tag.toLowerCase(), uuid }],
                ]
              : [...state];
          });
        }
      })
      .catch((error) =>
        addToast(`Произошла http ошибка: ${error.message}`, {
          appearance: 'error',
          autoDismiss: true,
        }),
      );
  };

  const changeGrouping = () => {
    setIsGrouped(!isGrouped);
  };

  const clearPhotos = () => {
    setPhotos([]);
    setTags([]);
    refInput?.current.setFieldValue('tag', '');
  };

  const setTagInInput = (tag: string) => {
    refInput?.current.setFieldValue('tag', tag);
  };

  return {
    photos,
    sendRequest,
    tags,
    isGrouped,
    changeGrouping,
    clearPhotos,
    refInput,
    setTagInInput,
  };
};
