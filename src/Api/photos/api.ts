import { transport } from 'Services/transport';

export type TGetPhotosRequest = {
  tag: string;
};

export const PhotosAPI = {
  getPhotos(params: TGetPhotosRequest) {
    return transport.get(params);
  },
};
