import React from 'react';
import { Gallery } from 'Pages/gallery';
import { ToastProvider } from 'react-toast-notifications';

function App() {
  return (
    <ToastProvider autoDismissTimeout={2500}>
      <Gallery />
    </ToastProvider>
  );
}

export default App;
